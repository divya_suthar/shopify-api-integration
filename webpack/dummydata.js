var path = require("path");
module.exports = {
	work: [
		{
			title: "DailyFresh",
			img: "dailyfresh.jpg",
			desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
		},
		{
			title: "BookWritten",
			img: "bookwritten.jpg",
			desc:
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."
		},
		{
			title: "Campus2Corporate",
			img: "campus2corporate.jpg",
			desc: "Lorem ipsum dolor sit amet, consectetur."
		},
		{
			title: "FilmyKeeday",
			img: "filmykeeday.jpg",
			desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
		},
		{
			title: "MoviesDrop",
			img: "moviesdrop.jpg",
			desc:
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod."
		},
		{
			title: "TravelClues",
			img: "travelclues.jpg",
			desc: "Lorem ipsum dolor sit amet, consectetur adipiscing."
		}
	],
	random(max) {
		return Math.floor(Math.random() * Math.floor(max));
	}
};

var path = require("path");
var webpack = require("webpack");
var htmlConfig = require("./html.config.js");
var dummyData = require("./dummydata.js");

const HtmlWebpackPlugin = require("html-webpack-plugin");
const ExtractCSS = require("extract-text-webpack-plugin");

function setPlugins() {
	var plugins = [
		new ExtractCSS({
			filename: "css/style.css"
		}),
		new webpack.NamedModulesPlugin()
	];

	//HTML Page Settings
	var htmlPluginOptions = htmlConfig.options;
	var viewPath = "./view/";
	htmlConfig.pages.forEach((item, index) => {
		htmlPluginOptions.filename = item.name + ".html";
		htmlPluginOptions.template = viewPath + item.name + ".pug";
		plugins.push(new HtmlWebpackPlugin(htmlPluginOptions));
	});

	return plugins;
}

/*
* 📣 : Pug Data & Variables
*/
function setPugData() {
	var pugData = {};
	pugData.dd = dummyData;
	pugData.img = "../img";
	return pugData;
}

module.exports = {
	entry: {
		"js/app": ["./js/app.js", "./scss/style.scss"]
	},
	output: {
		path: path.resolve(__dirname, "../dist"),
		filename: "[name].js"
	},
	module: {
		loaders: [
			{
				test: /\.pug$/,
				use: [
					{
						loader: "html-loader"
					},
					{
						loader: "pug-html-loader",
						options: {
							pretty: true,
							data: setPugData()
						}
					}
				]
			},
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				loader: "babel-loader"
			},
			{
				test: /\.vue$/,
				loader: "vue-loader"
			},
			{
				test: /\.(woff|woff2|eot|ttf)$/,
				loader: "file-loader",
				options: {
					outputPath: "fonts/",
					name: "[name].[ext]"
				}
			},
			{
				test: /\.(png|jpg|svg)$/,
				loader: "file-loader",
				options: {
					outputPath: "img/", //This will replace same image
					name: "[name].[ext]"
				}
			},
			{
				test: /\.scss$/,
				use: ["css-hot-loader"].concat(
					ExtractCSS.extract({
						fallback: "style-loader",
						use: [
							{ loader: "css-loader" },
							{
								loader: "postcss-loader",
								options: {
									config: {
										path: "./webpack/postcss.config.js"
									}
								}
							},
							{ loader: "sass-loader" }
						],
						publicPath: "../" //For Relative Path of Images
					})
				)
			},
			{
				test: /\.css$/,
				use: ["css-hot-loader"].concat(
					ExtractCSS.extract({
						fallback: "style-loader",
						use: [
							{ loader: "css-loader" },
							{
								loader: "postcss-loader",
								options: {
									config: {
										path: "./webpack/postcss.config.js"
									}
								}
							}
						],
						publicPath: "../"
					})
				)
			}
		]
	},
	plugins: setPlugins(),
	resolve: {
		alias: {
			// vue: 'vue/dist/vue.common.js',
			// styles: path.resolve(__dirname, './scss/'),
			// js: path.resolve(__dirname, './js/'),
			//imgPath: path.resolve(__dirname, "./img/")
		}
	}
};

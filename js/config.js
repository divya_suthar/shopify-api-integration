var siteConfig = {
	development: {
		env: "development"
	},
	debug: {
		env: "debug"
	},
	production: {
		env: "production"
	},
};

module.exports = siteConfig[process.env.NODE_ENV];

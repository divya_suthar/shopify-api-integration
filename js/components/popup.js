module.exports = {
	el:{
	},

	data:{
		currentPopup:[],
	},

	init:function(settings){
		var _this = this;
	
		_this.bindUIActions();
		if(settings){
			_this.data.settings = settings;
		}

		//Check URL for poup id
		if(window.location.hash){
			_this.open(window.location.hash.replace("#", ""));
		}
	},

	bindUIActions:function(){
		var _this = this;

		//When clicked on opener
		$(document).on("click",'[data-popup]',function(){
			if($(this).data('popup')){
				var settings = {}
				if($(this).data('popup-autoclose')){
					settings.autoclose = true;
				}
				_this.open($(this).data('popup'),settings);
			}
			else{
				alert('Please provide popup Id.');
			}
		});

		//When clicked on close
		$(document).on("click",'.popup__close',function(){
			_this.close($(this).parents('.popup').attr('id'));
		});

		//Clicked outside popup
		$(document).mouseup(function(e){
			_this.autoclose(e);
		});

		//Key Up
		$(document).keyup(function(e){
			_this.autoclose(e);
		});
	},

	autoclose:function(e){
		var _this = this;
		if(_this.data.currentPopup.length > 0){
			var popupID = _this.data.currentPopup[_this.data.currentPopup.length-1];
			if($('#'+popupID).hasClass('popup--autoclose')){
				var container = $('.popup__wrap');
				if(!container.is(e.target) && container.has(e.target).length === 0){
					_this.close();
				}
				else if(e.keyCode==27){
					_this.close();
				}
			}
		}
	},

	open:function(popupID,settings){
		var _this = this;
		var popupObj = $('#'+popupID);
		if(popupObj.length > 0){
			_this.data.currentPopup.push(popupID);
			popupObj.css('z-index',99 + _this.data.currentPopup.length).addClass('open');
			popupObj.find('.popup_focus').focus();

			$('body').addClass('popup__is-open');

			if(settings && settings.autoclose){
				popupObj.addClass('popup--autoclose');
			}
			if(_this.data.settings && _this.data.settings.afterOpen){
				_this.data.settings.afterOpen(popupID);
			}

		}
	},

	close:function(popupID){
		var _this = this;
		//If popupid is not defined set the last opened popup
		if(!popupID){
			popupID = _this.data.currentPopup[_this.data.currentPopup.length-1];
		}
		var popupObj = $('#'+popupID);
		if(popupObj.length > 0){
			var popupIndex = _this.data.currentPopup.indexOf(popupID);
			_this.data.currentPopup.splice(popupIndex, 1);
			popupObj.removeClass('open popup--autoclose');
			//popupObj.removeClass('open').css('z-index',-1);

			$('body').removeClass('popup__is-open');

			if(_this.data.settings && _this.data.settings.afterClose){
				_this.data.settings.afterClose(popupID);
			}
		}
		else{
			alert('Popup Not Found!');
		}
	},
};

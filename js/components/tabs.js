module.exports = (function( $, window, document, undefined ) {

	"use strict";

	var pluginName = "ssTabs",
	defaults = {
	};

	function Plugin (element,options) {
		this.element = element;
		this.settings = $.extend( {}, defaults, options );
		this._defaults = defaults;
		this._name = pluginName;
		this.init();
	}

	$.extend( Plugin.prototype, {

		init:function(){
			var _this = this;

			_this.set();
			_this.bindUIActions();
			if(_this.settings.afterInit){
				_this.settings.afterInit.call($(_this.element));
			}
		},

		data:{
			activeTab:0
		},

		bindUIActions:function(){
			var _this = this;

			$(_this.element).find('.ss-tabs__list > li > .ss-tabs__tab').on('click',function(){
				var tabNo = $(this).parent().index();
				if(tabNo != _this.data.activeTab){
					_this.changeTab(tabNo,'tab');
				}

			});

			$(_this.element).find('.ss-tabs__content > li > .ss-tabs__accordion').on('click',function(){
				var tabNo = $(this).parent().index();
				if(tabNo != _this.data.activeTab){
					_this.changeTab(tabNo,'accordion');
				}
			});

		},

		set:function(){
			var _this = this;
			var defaultTab = _this.data.activeTab;
			var target = 'tab';

			if(_this.settings.defaultTab){
				defaultTab = _this.settings.defaultTab;
			}
			if($(_this.element).hasClass('ss-tabs--accordion')){
				target = 'accordion';
			}
			_this.changeTab(defaultTab,target);

		},

		changeTab:function(tabNo,target){
			var _this = this;
			//Before Tab Change
			if(_this.settings.beforeChange){
				_this.settings.beforeChange($(_this.element));
			}

			if(tabNo % 1 === 0 && tabNo>=0){
				if(target && target=='tab'){
					_this.tabToggle(tabNo);
				}
				else if(target && target=='accordion'){
					_this.accordionToggle(tabNo);
				}
				else{
					_this.tabToggle(tabNo);
				}
			}
			else{
				console.log("Error!");
			}

			//After Tab Change
			if(_this.settings.afterChange){
				_this.settings.afterChange($(_this.element));
			}
		},

		tabToggle:function(tabNo){
			var _this = this;
			_this.data.activeTab = tabNo;
			$(_this.element).find('.ss-tabs__list > li').eq(tabNo).addClass('active').siblings().removeClass('active');
			$(_this.element).find('.ss-tabs__content > li').eq(tabNo).show().siblings().hide();
		},

		accordionToggle:function(tabNo){
			var _this = this;
			_this.data.activeTab = tabNo;
			var accordianHeader = $(_this.element).find('.ss-tabs__content > li');
			var accordianData = $(_this.element).find('.ss-tabs__content > li');
			if(_this.settings.singleOpen){
				accordianHeader.find('.ss-tabs__accordion').removeClass('active');
				accordianData.removeClass('active').find('.ss-tabs__data').slideUp(200);
			}
			accordianHeader.eq(tabNo).find('.ss-tabs__accordion').toggleClass('active');
			accordianData.eq(tabNo).toggleClass('active').find('.ss-tabs__data').slideToggle(200);
		},

	});

	$.fn[pluginName] = function ( options ) {
		var args = arguments;
		if (options === undefined || typeof options === 'object') {
			return this.each(function () {
				if (!$.data(this, 'plugin_' + pluginName)) {
					$.data(this, 'plugin_' + pluginName, new Plugin( this, options ));
				}
			});
		} else if (typeof options === 'string' && options[0] !== '_' && options !== 'init') {
			var returns;
			this.each(function () {
				var instance = $.data(this, 'plugin_' + pluginName);
				if (instance instanceof Plugin && typeof instance[options] === 'function') {

					returns = instance[options].apply( instance, Array.prototype.slice.call( args, 1 ) );
				}
				if (options === 'destroy') {
					$.data(this, 'plugin_' + pluginName, null);
				}
			});
			return returns !== undefined ? returns : null;
		}
	};

} )( jQuery, window, document );


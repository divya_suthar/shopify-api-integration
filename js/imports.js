/*
 * 📣 : Config Import From Config.js
 */
window.config = require("./config.js");

switch (window.config.env) {
	case "development":
		console.log(
			"😱 You're running in Development Mode! Consider to enable Production Mode when deploying."
		);
		break;
	case "debug":
		console.log(
			"😱 You're running in Debug Mode! Consider to enable Production Mode when deploying."
		);
		break;
	case "production":
		break;
	default:
}

/*
 * 📣 : Components & Libraries Import
 */

//jQuery
window.$ = window.jquery = window.jQuery = require("jquery");
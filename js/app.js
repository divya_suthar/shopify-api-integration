require("./imports.js");

/*
 * 📣 : After Page Load
 */
$(function () {
	//Config
	console.log(config);

	var variant = [];
	$.ajax({
		type: "GET",
		dataType: 'json',
		url: baseUrl + "products/" + productName,
		success: function (response) {
			var productSize = response.options[0].values;
			var sizeSelect = $('.jq_product_size');
			var productColor = response.options[1].values;
			var colorRadio = $('.jq_product_color');
			var totalVariants = response.variants;
			variant = totalVariants;
			$('.jq_product_title').text(response.title);
			$('.jq_product_price_sell').text('$' + response.price / 100);
			$('.jq_product_price_old').text('$' + response.price_max / 100);
			$('.jq_product_description').html(response.description);
			$('.jq_product_image').attr('src', response.featured_image);
			$.each(productSize, function (val, text) {
				$(sizeSelect).append(
					$('<option></option>').val(text).html(text)
				);
			});
			$.each(productColor, function (val, text) {
				$("input:radio[name='color']:first").attr('checked', true);
				$(colorRadio).append(
					$("<label class='product-color__checkbox'><input type='radio' value='" + text + "' name='color'/><span class='checkmark' style='background-color:" + text + "'></span></label>")
				);
			});
		}
	});
	$(".jq_product_color").change(function () {
		var color = $("input:radio[name='color']:checked").val();
		var size = $("select.jq_product_size").val();
		var pr = getVariant(color, size, price = "true");
		$('.jq_product_price_sell ').text('$' + pr / 100);
	});

	$(".jq_product_size").change(function () {
		var color = $("input:radio[name='color']:checked").val();
		var size = $("select.jq_product_size").val();
		var pr = getVariant(color, size, price = "true");
		$('.jq_product_price_sell ').text('$' + pr / 100);
	});

	$("#buy_now").click(function () {
		var colorValue = $("input:radio[name='color']:checked").val();
		var sizeValue = $("select.jq_product_size").val();
		var variantId = getVariant(colorValue, sizeValue);
		var qty = $(".jq_product_quntity input[type='text']").val();
		$(this).attr('href', baseUrl + "cart/" + variantId + ":" + qty + "?checkout");
	});
	$("#add_cart").click(function () {
		var colorValue = $("input:radio[name='color']:checked").val();
		var sizeValue = $("select.jq_product_size").val();
		var variantId = getVariant(colorValue, sizeValue);
		var qty = $(".jq_product_quntity input[type='text']").val();
		$(this).attr('href', baseUrl + "cart/add?id=" + variantId + "&quantity=" + qty);
	});


	function getVariant(color, size, price = "false") {
		var rst = "";
		$.each(variant, function (key, val) {
			if (color == val.option2 && size == val.option1) {
				if (price == "true") {
					rst = val.price;
				} else {
					rst = val.id;
				}
			}
		});
		return rst;
	}

});
var unit = 1;
var total;
// if user changes value in field
$('.quantity').change(function () {
	unit = this.value;
});
$('.add').click(function () {
	unit++;
	var $input = $(this).prevUntil('.sub');
	$input.val(unit);
	unit = unit;
});
$('.sub').click(function () {
	if (unit > 1) {
		unit--;
		var $input = $(this).nextUntil('.add');
		$input.val(unit);
	}
});